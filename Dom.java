package dom;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class Dom {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File file = new File("test.xml");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(file);

	
	TreeNode n = new TreeNode("root");
	ParseToTree(document, n);
	

	System.out.println(n.getNode("title"));

	}


	
	public static void ParseToTree(Node in, TreeNode out) {
		if (in == null) {
			return;
		}
		if (!in.getNodeName().equals("#text")) {
			if (in.getNodeName().equals("#document")) {
				out.element = in.getNodeName();
			}
			if (in.getParentNode() != null) {
				String path = in.getParentNode().getNodeName();
				TreeNode n = new TreeNode();
				n.setElement(in.getNodeName());
				if(!in.getTextContent().isEmpty()){
					n.text=in.getTextContent();
				}
				if (in.getAttributes().getLength() != 0) {
					for (int j = 0; j < in.getAttributes().getLength(); j++) {
						String key = in.getAttributes().item(j).getNodeName();
						String value = in.getAttributes().item(j).getNodeValue();
						n.getAttributes().put(key, value);
					}
				}
				
				out.addNode(path, n);
			}
		}

		for (int i = 0; i < in.getChildNodes().getLength(); i++) {
			ParseToTree(in.getChildNodes().item(i), out);
		}
	}

}
