package dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeNode {
	public TreeNode perent;
	public String element = "";
	public String text = "";
	public Map<String, String> attributes = new HashMap<>();
	public List<TreeNode> children = new ArrayList<>();

	TreeNode() {
	}

	TreeNode(String element) {
		super();
		this.element = element;

	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	// -------------------------------------------
	public void addNode(String perentName, TreeNode newNode) {

		add(this, perentName, newNode);

	}

	private void add(TreeNode root, String element, TreeNode newNode) {
		if (root == null) {
			return;
		}
		if (root.getElement().equals(element)) {
			newNode.perent = root;
			root.getChildren().add(newNode);
			return;
		}

		for (int i = 0; i < root.children.size(); i++) {
			add(root.children.get(i), element, newNode);

		}
	}

	// -----------------------------------------------------
	public TreeNode getNode(String element) {
		return get(this, element);
	}

	public TreeNode get(TreeNode node, String element) {
		if (node == null){
			return null;}
		if (node.element.equals(element)) {
			return node;
		}
		for (int i = 0; i < node.children.size();i++) {
			 get(node.children.get(i), element);
		}
		
		return null;

	}

	// ------------------------------------------
	public void order(TreeNode node) {
		if (node == null)
			return;
		if (node.perent == null) {
			System.out.println("THE ROOT=" + node.element);

		} else if (node.perent != null) {
			if (!node.attributes.isEmpty()) {
				System.out.println("Attribute:" + node.attributes.keySet() + " value:" + node.attributes.values());
			}
			//node can contains text when he hasn't children  
			if(node.text!=""){
				if(node.children.isEmpty()){
					System.out.println("Text value is:"+node.text);
				}
			}
			
			
			System.out.println("node:" + node.element + " perent:" + node.perent.element);
		
		}
		
		System.out.println("---------------------------");

		for (int i = 0; i < node.children.size(); i++) {
			order(node.children.get(i));
		}
	}

}
